/* Um Traveler (Viajante) tem algumas propriedades: * um name (nome) (string) que deve ser fornecido como parâmetro para o construtor.
* uma quantidade de food (comida) (número) com valor inicial 1. * uma propriedade isHealthy (booleano) que indica se ele está doente
com valor inicial true. */

function Traveler(name, food, isHealthy) {
    this.name = name;
    this.food = 1;
    this.isHealthy = true;
}

Traveler.prototype = {
    constructor: Traveler,
    hunt: function() { //Aumenta a comida do viajante em 2.
        return this.food += 2
    },
    eat: function() { //Consome 1 unidade da comida do viajante. Se o viajante não tiver comida para comer, ele deixa de estar saudável.
        if (this.food !== 0) {
            return this.food -= 1
        } else {
            return this.isHealthy = false
        }
    }
}

/* Uma Wagon que também tem algumas propriedades: * uma capacity (capacidade) (número), que deve ser fornecida como
parâmetro para o construtor, determina a quantidade máxima de passageiros que cabe na carroça. * uma lista de passageiros 
(array) que inicialmente está vazia. */

function Wagon(capacity, passengers) {
    this.capacity = capacity; //número máximo 
    this.passengers = [] //assentos ocupados
}

Wagon.prototype = {
    constructor: Wagon,
    getAvailableSeatCount: function() { //retorna o número de assentos vazios, determinado pela capacidade que foi estipulada quando a carroça foi criada comparado com o número de passageiros a bordo no momento.
        let result = this.capacity - this.passengers.length
        return result
    },
    join: function(traveler) { //Adicione o viajante à carroça se tiver espaço. Se a carroça já estiver cheia, não o adicione.
        if (this.capacity > this.passengers.length) {
            return this.passengers.push(traveler)
        }
    },
    shouldQuarantine: function() { //Retorna true se houver pelo menos uma pessoa não saudável na carroça. Retorna false se não houver.
        for (let i = 0; i < this.passengers.length; i++) {
            if (this.passengers[i].isHealthy === false) {
                return true
            }
        }
    },
    totalFood: function() { //Retorna o número total de comida de todos os ocupantes da carroça. 
        let passengersFood = 0
        for (let i = 0; i < this.passengers.length; i++) {
            passengersFood += this.passengers[i].food
        }
        return passengersFood
    }
}


// Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');
console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);